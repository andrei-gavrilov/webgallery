<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtworkCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artwork_categories', function (Blueprint $table) {
            $table->integer('artwork_id')->unsigned(); 
            $table->integer('category_id')->unsigned(); 

            $table->index(['artwork_id', 'category_id']);
            $table->foreign('artwork_id')->references('id')->on('artworks');
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artwork_categories');
    }
}
