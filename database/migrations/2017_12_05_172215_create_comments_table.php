<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('text', 2000);
            // $table->integer('reply')->unsigned()->nullable();
            $table->timestamp('date');
            $table->smallInteger('like_count')->unsigned()->default(0);
            $table->integer('artwork_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('artwork_id')->references('id')->on('artworks');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
