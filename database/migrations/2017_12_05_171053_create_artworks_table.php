<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('artworks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 30);
            $table->string('description', 2000)->nullable();
            $table->timestamp('upload_date');
            $table->smallInteger('like_count')->unsigned()->default(0);
            $table->smallInteger('view_count')->unsigned()->default(0);
            $table->string('file_path', 200)->unique();
            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artworks');
    }
}
