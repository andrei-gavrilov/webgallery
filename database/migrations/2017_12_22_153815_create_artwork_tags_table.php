<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtworkTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artwork_tags', function (Blueprint $table) {
            $table->integer('artwork_id')->unsigned(); 
            $table->integer('tag_id')->unsigned(); 

            $table->index(['artwork_id', 'tag_id']);
            $table->foreign('artwork_id')->references('id')->on('artworks');
            $table->foreign('tag_id')->references('id')->on('tags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artwork_tags');
    }
}
