<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 1)->states('adm')->create();
        factory(App\User::class, 2)->states('mod')->create();
        factory(App\User::class, 7)->create();

    }
}
