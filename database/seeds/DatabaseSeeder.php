<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(TypesTableSeeder::class);
        $this->call(TypeUsersTableSeeder::class);

        $this->call(CategoriesTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(ArtworksTableSeeder::class);

        $this->call(ArtworkCategoriesTableSeeder::class);
        $this->call(ArtworkTagsTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        
    }
}
