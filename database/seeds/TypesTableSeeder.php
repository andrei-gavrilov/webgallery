<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            ['name' => 'Digital artist'],
            ['name' => 'Traditional artist'],
            ['name' => 'Professional artist'],
            ['name' => 'Photographer'],
            ['name' => 'Hobbyist']
        ]);
    }
}
