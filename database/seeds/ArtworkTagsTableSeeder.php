<?php

use Illuminate\Database\Seeder;

class ArtworkTagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ArtworkTag::class, 20)->create();
    }
}
