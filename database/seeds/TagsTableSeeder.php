<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('tags')->insert([
            ['name' => 'pie'],
            ['name' => 'titanic'],
            ['name' => 'love'],
            ['name' => 'asia'],
            ['name' => 'night'],
            ['name' => 'clock'],
            ['name' => 'winter'],
            ['name' => 'tea'],
            ['name' => 'glasses'],
        ]);
    }
}
