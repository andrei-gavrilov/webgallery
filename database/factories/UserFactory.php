    <?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'role' => "usr",
        'remember_token' => str_random(10),
    ];
});

$factory->state(App\User::class,'adm', [
        'role' => "adm",
]);

$factory->state(App\User::class,'mod', [
    'role' => "mod",
]);


$factory->define(App\Artwork::class, function (Faker $faker) {
    $image=$faker->image('storage/app/public/artworks',800, 600, 'cats',false);
    
    return [
        'name' => $faker->text($maxNbChars = 10),
        'description' => $faker->text($maxNbChars = 100),
        //'file_path' => $faker->imageUrl(800, 600, 'cats'),
        'file_path' => $image,
        'user_id' => $faker->numberBetween($min = 1, $max = 10),
    ];
});

$factory->define(App\ArtworkCategory::class, function (Faker $faker) {
    return [
        'artwork_id' => $faker->numberBetween($min = 1, $max = 10),
        'category_id' => $faker->numberBetween($min = 1, $max = 5),
    ];
});

$factory->define(App\ArtworkTag::class, function (Faker $faker) {
    return [
        'artwork_id' => $faker->numberBetween($min = 1, $max = 10),
        'tag_id' => $faker->numberBetween($min = 1, $max = 9),
    ];
});

$factory->define(App\TypeUser::class, function (Faker $faker){
    static $number = 1;
    return [
        'user_id' => $number++,
        'type_id' => $faker->numberBetween($min = 1, $max = 5),
    ];
});

$factory->define(App\Comment::class, function (Faker $faker){
    static $number = 1;
    return [
        'text' => $faker->text($maxNbChars = 250),
        'artwork_id' => $faker->numberBetween($min = 1, $max = 20),
        'user_id' => $faker->numberBetween($min = 1, $max = 10),
    ];
});


