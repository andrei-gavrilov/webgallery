@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">User profiles</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-bordered table-responsive">
                        <tbody>
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Role</th>
                                <th scope="col">Action</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        @foreach($user as $u)
                        <tr>
                            <td>{{ $u['name'] }}</td>
                            <td>{{ $u['role'] }}</td>
                            <td><a href="{{route('user.edit', ['id' => $u['id']]) }}">Edit</a></td>
                            <td><a href="{{route('user.destroy', ['id' => $u['id']]) }}">Delete</a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $user->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection