@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $user->name }}'s profile edit page</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-bordered table-responsive">
                        <tbody>
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">Link</th>
                            </tr>
                        </thead>
                        @foreach($user->artwork as $a)
                        <tr>
                            <td>{{ $a['name'] }}</td>
                            <td>{{ $a['description'] }}</td>
                            <td><a href="{{ $a['file_path'] }}">Link</a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection