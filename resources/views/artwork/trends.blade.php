@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Trendy artworks</div>
                
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @foreach($artworks as $artwork)
                    <div class="panel-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                            {{ $artwork->upload_date }},
                            {{ $artwork->name }}
                            </div>
                            <div class="col-md-6 text-right">
                            <a href="{{route('user.show', ['id' => $artwork->user_id])}}">
                            {{ $artwork->user->name }}
                            </a>
                            </div>
                        </div>
                        </div>
                        <div class="panel-body">    
                            <h2>{{ $artwork->name }}</h2>
                            <a href="{{route('artwork.show', ['id' => $artwork->id])}}">
                                <img class="img-responsive" src="storage/artworks/{{ $artwork->file_path }}" alt="{{ $artwork->name }}" style="padding-top:30px!important; padding-bottom:30px!important">
                            </a >
                            {{ str_limit($artwork->description, 200) }}
                            <p><a href="{{route('artwork.show', ['id' => $artwork->id])}}" style="color:#13afea!important">Read more</a></p>
                        </div>
                        <div class="panel-footer">
                                @foreach($artwork->artwork_categories as $category)
                                <a href="#" class="badge badge-primary">{{ $category->category->name }}</a>
                                @endforeach
                                @foreach($artwork->artwork_tags as $tags)
                                <a href="#" class="badge badge-success">{{ $tags->tag->name }}</a>
                                @endforeach
                        </div>
                    </div>
                    </div>
                    @endforeach
                </div>
                <div class="panel-footer">
                {{ $artworks->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection