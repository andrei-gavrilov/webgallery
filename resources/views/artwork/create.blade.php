@extends('layouts.app')

@section('content')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">

<div class="container">
   <div class="row">
       <div class="col-md-10 col-md-offset-1">
           <div class="panel panel-default">
               <div class="panel-heading">Artwork submission</div>

               <div class="panel-body">
               @if (count($errors) > 0)
                   <div class="alert alert-danger">
                   <ul>
                       @foreach ($errors->all() as $error)
                           <li>{{ $error }}</li>
                       @endforeach
                   </ul>
               </div>
               @endif
               <form class="form-horizontal" action="{{ route('artwork.store') }}" method="POST"  enctype="multipart/form-data">
                       {{ csrf_field() }}
                       <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                           <label for="title" class="col-md-4 control-label">Title</label>
                           <div class="col-md-6">
                               <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required autofocus>
                               @if ($errors->has('title'))
                                   <span class="help-block">
                                       <strong>{{ $errors->get('title') }}</strong>
                                   </span>
                               @endif
                           </div>
                       </div>
                       <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                           <label for="description" class="col-md-4 control-label">Description</label>
                           <div class="col-md-6">
                               <textarea id="description" type="text" class="form-control" name="description" rows="15"  required autofocus>{{ old('description') }}</textarea>
                               @if ($errors->has('description'))
                                   <span class="help-block">
                                       <strong>{{ $errors->get('description') }}</strong>
                                   </span>
                               @endif
                           </div>
                       </div>
                       <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                           <label for="artwork" class="col-md-4 control-label">Artwork</label>
                           <div class="col-md-6">
                           <input class="field" id="artwork" name="artwork"  type="file">
                               @if ($errors->has('file'))
                                   <span class="help-block">
                                       <strong>{{ $errors->get('description') }}</strong>
                                   </span>
                               @endif
                           </div>
                       </div>
                       <div class="form-group">
                           <label for="category" class="col-md-4 control-label">Select category:</label>  
                           <div class="col-md-6">


                          
                                  
                                   <select id="category" multiple="multiple" name="category">
                                   @foreach($categories as $category)
                                   <option value="{{$category->id}}"}}  {{ old('category')==$category->id?"selected":"" }}     >{{$category->name}}</option>
                                   @endforeach
                               </select>
                               </div>
                              
                               <script type="text/javascript">
                                   $(document).ready(function() {
                                       $('#category').multiselect();
                                   });
                               </script>


                          
                      
                       </div>
                       <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                           <label for="tags" class="col-md-4 control-label">Tags</label>
                           <div class="col-md-6">
                               <input id="tags" type="text" class="form-control" name="tags" value="{{ old('tags') }}" autofocus>
                               @if ($errors->has('tags'))
                                   <span class="help-block">
                                       <strong>{{ $errors->get('name') }}</strong>
                                   </span>
                               @endif
                           </div>
                       </div>
                       <div class="form-group">
                           <div class="col-md-8 col-md-offset-4">
                               <button type="submit" class="btn btn-primary">
                                   Submit
                               </button>
                           </div>
                       </div>
                       </div>
                   </form>
               </div>
           </div>
       </div>
   </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css">


@endsection