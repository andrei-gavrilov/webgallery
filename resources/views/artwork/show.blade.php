@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Artwork page!</div>
                <div class="panel-body">
                    <div class="panel panel-primary">
                    <div class="panel-heading">Artwork Name: {{ $artwork->name }}</div>
                    <div class="panel-body">
                    <div class="panel-body">

                 

                        <div class="panel-body">    
                            <a href="{{route('artwork.show', ['id' => $artwork->id])}}">
                                <img class="img-responsive" src="{{URL::to('/')}}/storage/artworks/{{ $artwork->file_path }}" alt="{{ $artwork->name }}">
                            </a>

                    </div>
                
                                            
                    <table class="table table-bordered table-responsive">

                    
                        <tbody>
                        

                            <tr>
                                <td>description</td>
                                <td>
                                @if(empty($artwork->description))
                                    No description.
                                @else
                                    {{ $artwork->description }}
                                @endif
                                </td>
                            </tr>
                            <tr>
                                <td>upload date</td>
                                <td>{{ $artwork->upload_date }}</td>
                            </tr>
                            <tr>
                                <td>likes</td>
                                <td>{{ $artwork->like_count }}</td>
                            </tr>
                            <tr>
                                <td>views</td>
                                <td>{{ $artwork->view_count }}</td>
                            </tr>
                            <tr>
                                <td>file path</td>
                                <td>{{ $artwork->file_path }}</td>
                            </tr>
                            <tr>
                                <td>Uploader</td>
                                <td>{{ $artwork->user->name }}</td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                    <div class="panel-footer">
                        @auth
                         <a href="" style="color:#13afea!important">Leave a like</a>
                        @else
                            <a href="{{route('login')}}" style="color:#13afea!important">Log in</a> or <a href="{{route('register')}}" style="color:#13afea!important">Register</a> to leave a comment
                        @endauth
                    </div>
                    </div>
                    <p><h2>Categories:</h2></p>
                    @if(!$artwork->artwork_categories->isEmpty())
                        @foreach($artwork->artwork_categories as $category)
                        <a href="#" class="badge badge-primary">{{ $category->category->name }}</a>
                        @endforeach
                    @else
                        Owww! No categories!
                    @endif
                    <p><h2>Tags:</h2></p>
                    @if(!$artwork->artwork_tags->isEmpty())
                        @foreach($artwork->artwork_tags as $tags)
                        <a href="#" class="badge badge-primary">{{ $tags->tag->name }}</a>
                        @endforeach
                    @else
                        Owww! No tags!
                    @endif
                    
                    


                    {{ $с['text'] }}, {{ $с['date'] }}, {{ $с['like_count'] }}</br>

                    @endforeach
                    <textarea id="comment" type="text" class="form-control" name="description" rows="15"  required autofocus> </textarea>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection