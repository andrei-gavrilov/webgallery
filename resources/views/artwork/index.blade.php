@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">All artworks</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    
                <div class="row">
                @foreach($artworks as $a)
                    <div class="col-xs-6 col-md-3">
                        <a href="{{route('artwork.show', ['id' => $a->id])}}" class="thumbnail">
                        <img src="storage/artworks/{{ $a['file_path'] }}" alt="...">
                        </a>
                </div>
                @endforeach
                
                    </div>
                    {{ $artworks->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection