<div class="panel-body">
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif


<div class="row">
@foreach($artworks as $a)
<div class="col-xs-6 col-md-3">
    <a href="{{route('artwork.show', ['id' => $a->id])}}" class="thumbnail">
    <img src="storage/artworks/{{ $a['file_path'] }}" alt="...">
    </a>
</div>
@endforeach

</div>
{{ $artworks->links() }}
</div>