@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Home dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <p>You are logged in!</p>
                    <p><a class="btn btn-primary" href="{{route('artwork.create')}}" role="button">Submit artwork</a></p>
                            <div class="panel panel-default">
                        <div class="panel-heading">Your recent artworks</div>

                        <div class="panel-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            
                            
                        <div class="row">

                        @unless (count($artworks))
                            <div class="col"><h3 style="text-align:center!important;">You have no artworks yet!</h3></div>
                        @endunless
                            @foreach($artworks as $a)
                                <div class="col-xs-6 col-md-3">
                                    <a href="{{route('artwork.show', ['id' => $a->id])}}" class="thumbnail">
                                    <img src="storage/artworks/{{ $a['file_path'] }}" alt="...">
                                    </a>
                                    <a href="{{route('artwork.edit', ['id' => $a->id])}}">edit</a>
                            </div>
                            @endforeach
                            </div>
                            {{ $artworks->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
