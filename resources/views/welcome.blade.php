@extends('layouts.app')

@section('content')
<h1 style="text-align: center!important; color:#999; padding-top:30px; padding-bottom:30px">Welcome!</h1>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Intorduction</div>
                <div class="panel-body">
                    <p>"Hello everyone! This is our project Artgallery and we are glad to present it to you from behalf of our team!
                         This is a project for our College made by Janar Tomband, Andrei Gavrilov, Vladimir Andrianov.
                         We made this to spread art and all its forms through out the internet and we hope that you will enjoy your stay!
                         "</p>

                         <img class="center" src="{{URL::to('/')}}/picture_ref/1.jpg"   width="400px" length="400px" align="middle!important" style="padding-right:-15px; opacity: 0.7;">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <script type="text/javascript" src="jquery-1.4.2.js"></script>
<script type="text/javascript" src="coin-slider.min.js"></script>
<link rel="stylesheet" href="coin-slider-styles.css" type="text/css" />

<div id='coin-slider'>
  <a href="img01_url" target="_blank">
    <img src="{{URL::to('/')}}/picture_ref/Icon5.png" >
  </a>
  <a href="imgN_url">
    <img src="{{URL::to('/')}}/picture_ref/Icon5.png" >
  </a>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#coin-slider').coinslider();
  });
</script> -->

@endsection