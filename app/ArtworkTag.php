<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtworkTag extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'artwork_id','tag_id',
    ];
    public function tag()
    {
        return $this->belongsTo('App\Tag');
    }
    public function artwork()
    {
        return $this->belongsTo('App\Artwork');
    }
}
