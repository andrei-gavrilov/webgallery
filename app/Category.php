<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;
    public function artwork_categories()
    {
        return $this->hasMany('App\ArtworkCategory');
    }
}
