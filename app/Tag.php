<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'name','count',
    ];
    public $timestamps = false;
    public function artwork_tags()
    {
        return $this->hasMany('App\ArtworkTag');
    }
}
