<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
class ArtworkCategory extends Model
{
    use SearchableTrait;
    public $timestamps = false;
    protected $fillable = [
        'category_id','artwork_id',
    ];
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
    public function artwork()
    {
        return $this->belongsTo('App\Artwork');
    }
}
