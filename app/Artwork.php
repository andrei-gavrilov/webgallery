<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
class Artwork extends Model
{
    use SearchableTrait;
    
    public $timestamps = false;
    protected $searchable = [
        'columns' => [
            'artworks.name' => 10,
            'artworks.description' => 5,
            'users.name'=>5,
        ],
        'joins' => [
            'users' => ['users.id','artworks.user_id'],
        ],
        
    ];
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
    public function artwork_categories()
    {
        return $this->hasMany('App\ArtworkCategory');
    }
    public function artwork_tags()
    {
        return $this->hasMany('App\ArtworkTag');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
   }
