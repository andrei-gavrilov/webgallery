<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Nicolaslopezj\Searchable\SearchableTrait;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function type_users()
    {
        return $this->hasMany('App\TypeUser');
    }
    public function artwork()
    {
        return $this->hasMany('App\Artwork');
    }
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
    public function isAdmin()
    {
        if($this->role=="adm") {
            return true;
        }
        else{
            return false;
        }
    }
}
