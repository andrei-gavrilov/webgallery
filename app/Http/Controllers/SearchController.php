<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artwork;

class SearchController extends Controller
{
    public function search(Request $request) {
                if($request->has('searchText')){
                    $artworks = Artwork::search($request->searchText)->paginate(12);	
                }else{
                    $artworks = Artwork::paginate(12);
                }


                return view('artwork.search',['artworks' => $artworks]);
            }
}
