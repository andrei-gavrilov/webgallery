<?php

namespace App\Http\Controllers\Auth;

use App\User;
use \DB;
use App\Type;
use App\Tag;
use App\ArtworkTag;
use App\TypeUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'types' => 'required|array|min:1',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //\DB::beginTransaction();
        $user=User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'role' => "usr",
            'password' => bcrypt($data['password']),
        ]);

        foreach ($data['types'] as $type){
            TypeUser::create([
                'user_id' => $user->id,
                'type_id' => $type,
            ]);
        }
        

        
        return $user;


        /*return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'role' => "usr",
            'password' => bcrypt($data['password']),
        ]);*/
        //Storage::makeDirectory();
    }
}
