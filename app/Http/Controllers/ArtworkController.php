<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Artwork;
use App\ArtworkCategory;
use App\ArtworkTag;
use App\Tag;
use Carbon\Carbon;
class ArtworkController extends Controller
{



    public function __construct()
    {
        $this->middleware('auth', ['only' => ['create', 'store', 'delete','edit','update']]);
    }

    public function index()
    {
        return view('artwork.index', ['artworks' => Artwork::paginate(12)]);
    }
    
    public function create()
    {
        $categories=\App\Category::all();
        return view('artwork.create')->with('categories', $categories);
    }
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required|max:30',
            'description'=>'max:2000',
            'category'=>'required',
            'artwork' => 'required|file|mimes:jpeg,bmp,png|max:10000',
        ]);
        $artwork=new Artwork;
        $artwork_categories=new ArtworkCategory;

        $artwork->name=$request->title;
        $artwork->description=$request->description;
        $path=$request->file('artwork')->store('public/artworks');
        $artwork->file_path=basename($path);
        $artwork->user_id=\Auth::user()->id;
       
        
        $artwork->save();

        $artwork_categories->artwork_id=$artwork->id;
        $artwork_categories->category_id=$request->category;
        $artwork_categories->save();

        $words = preg_split('/\s+/', $request->tags, -1, PREG_SPLIT_NO_EMPTY);

        foreach($words as $rawTag){
        $tag = Tag::where('name', $rawTag)->first();
                        if ($tag==null ){
                            $tag=Tag::create([
                                'name' => $rawTag,
                                'count'=>1,
                            ]);
                        }
                        else{
                            $tag->count++;
                            $tag->save();
                        }
                        ArtworkTag::create([
                            'artwork_id' => $artwork->id,
                            'tag_id'=>$tag->id,
                        ]);
        }
        return redirect()->route('home');
    }



    public function show($id)
    {
        return view('artwork.show', ['artwork' => Artwork::find($id)]);
    }

    public function edit($id)
    {
        $categories=\App\Category::all();
        $artworks=Artwork::findOrFail($id);
        //$artwork_tags=ArtworkTag::where('artwork_id','=',$artwork->id)->get();
        $tags="";
        foreach($artworks->artwork_tags as $artwork_tag){
            //$tags.=$artwork_tag->tag_id;
            $tags.=$artwork_tag->tag->name." ";
        }
        return view('artwork.edit', ['artwork' => $artworks])->with('categories', $categories)->with('tags', $tags);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required|max:30',
            'description'=>'max:2000',
            'category'=>'required'
        ]);
        $artwork = App\Artwork::find($id);
        $artwork->name=$request->title;
        $artwork->description=$request->description;
        $artwork->save();

        
        
        $artwork_tags_prev=App\ArtworkTag::where('artwork_id','=',$artwork->id)->get();
        $artwork_categories_prev=App\ArtworkCategory::where('artwork_id','=',$artwork->id)->get();
	

        /////adds new tags and its relations with artwork
        $words = preg_split('/\s+/', $request->tags, -1, PREG_SPLIT_NO_EMPTY);
                foreach($words as $rawTag){
                $tag = Tag::where('name', $rawTag)->first();
                                if ($tag==null ){//if tag does not exists
                                    $tag=Tag::create([
                                        'name' => $rawTag,
                                        'count'=>1,
                                    ]);
                                    ArtworkTag::create([
                                        'artwork_id' => $artwork->id,
                                        'tag_id'=>$tag->id,
                                    ]);
                                    
                                }
                                else{//if exists
                                    $artwork_has_tag=false;
                                    foreach($artwork_tags_prev as $artwork_tag_prev){//cycles through artworks tags  before update
                                        if($artwork_tag_prev==$tag){
                                            $artwork_has_tag=true;
                                            break;
                                        }
                                    }
                                    if($artwork_has_tag){//if tag already exists and artwork has it
                                        $artwork_tag_prev->delete();
                                    }
                                    else{//if tag already exists and artwork doesnt have it
                                        $tag->count++;
                                        $tag->save();
                                        ArtworkTag::create([
                                            'artwork_id' => $artwork->id,
                                            'tag_id'=>$tag->id,
                                        ]);
                                        
                                    }
                                    
                                }
                               
        }
        return redirect()->route('home');
    }

    public function destroy($id)
    {
        $artwork=Artwork::findOrFail($id);
        $artwork->delete();
        return view('home');
    }
    public function fresh()
    {
        return view('artwork.index', ['artworks' => Artwork::orderBy('upload_date', 'desc')->paginate(12)]);
    }
    public function trends()
    {
        $date= Carbon::now()->toDateString();
        return view('artwork.trends', ['artworks' => Artwork::whereDate('upload_date',$date)->orderBy('like_count', 'desc')->paginate(12)]);
    }
    
}
